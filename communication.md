## code of communications 

At the root of our values, Codesmith believes communication and visibility is the key to success and mending any potential damage.

+ Communicate early and often especially about any risks involved in a process or project.
+ Take the initiative to communicate, making sure you understand what is communicated to you.
+ With any client (or internal) issues, immediately acknowledge receipt of the news/task and provide a timeline for a follow-up reply or resolution.
+ Minimize personal communication during the workday so that we can focus on our work and are free to focus on our families, friends, and outside interests when we are not at work.
+ Communicate bad news face to face or via phone. We do not use email for communicating bad news or anything that should require a discussion.
+ No one should be obligated to check or respond to an email after hours or on the weekend. If there is an emergency, you will be notified via text or a phone call.
