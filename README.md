# welcome

Hey! We're glad you're here. You're are extremely valuable to the Codesmith team, and this Handbook will serve as a great resource for the knowledge you'll need to be successful here. We're going to outline what it takes to be a great employee, teammate, and client partner at Codesmith. 

# what we believe

### our mission 

Our mission is to enjoy what we do, and deliver a happy work-life balance. 

### our vision

Our vision is for Codesmith to become the most respected and sought-after software professional services agency in Atlanta. 

### our driving belief 

At Codesmith, we believe in the “concierge” approach to what we do. This involves high communication, high visibility and cost benefit solutions. We deliver on time and on budget. Converting clients to partners is the end game for every project.

### our values 

Our value system is the secret recipe for our success and developing long term partnerships. 

+ **COMMUNICATION**:
We always make sure we're on the same page.

+ **VISIBILITY**:
We make sure you are part of the process. 

+ **IMPACT**:
We move the productivity meter.

+ **ENGAGE**:
We understand, deliver, and drive. 

### what to expect from Codesmith

  + You are expected to work with the different groups and collaborate regularly. We believe that internal discussions fuels great solutions and thought-provoking ideas.

  + We are committed to treating team members, vendors, and clients the way we would want to be treated.

  + We want to build relationships that ensure our mutual success. We collaborate as a team committed to providing incredible service. 

  + We will help both clients and teammates be successful.

  + We will operate a profitable business that creates opportunities for professional growth and performance bonuses. 

  + We provide a fair, honest, and open environment where the highest levels of integrity and excellence are expected.

  + When one of us fails, we all fail. 

  + We use terms like “we” and “us”, never “I”.

  + We will provide eligible employees with benefits such as paid vacation, holidays and other opportunities.

  + We select people based on skill, training, ability, attitude and character without discrimination with regard to age, gender, sex, color, race, creed, national origin, religion, marital status, or disability that does not prohibit performance of essential job functions. We welcome a celebrate diversity of background and experience, and will do everything we can to support our team. 
