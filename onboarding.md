# let's get acquainted 

Onboarding is a 90-day experience for new employees at Codesmith, where you are immersed in our culture and processes, and nurtured into the technical skills you need to be successful on the team long-term.

### orientation 

Your first week orientation serves a couple of main purposes: 

+ Set you up with the tools you need to be successful in your role, learn the team, and who is here to help you
+ Get over the awkward hump of getting to know a new team, new clients, and new expectations. Break the ice and get to feeling like you're at home!

Here's a high level view of what you'll pick up your first week at Codesmith: 

+ Overview of company history, the "why" behind what we do here, and a view of our clients
+ Get to know our Atlanta, NY, and other remote teammates 
+ Familiarization with our project management, communication, development, and productivity tools 
+ Context behind your first project and the technical parameters
+ Learn the process full-cycle: how we engage with clients, how we form project plans, and how we deliver
+ Tooling best practices 
+ Mentorship opportunities with our President 

At the end of your first week, you should: 

+ Feel comfortable asking for help 
+ Know your goals at Codesmith, and how to attain those 
+ Feel comfortable talking about Codesmith, our process, and how we forge meaningful partnerships with our clients

### onboarding

#### day 30 

You are now ready to be trained and coached to become a great partner for our clients.

  **Step 1:** You will be taught which SaaS applications are used in order to do your job effectively and start keeping track of data useful to the team.

  **Step 2:** You will be part of client discussions, so that you learn our process of communicating with clients and what behaviors we expect while engaging our clients.

  **Step 3:** We will only play to your strengths. You were hired for a purpose and we want to make sure you are always set up for success and “winning” at what you do, thus the team wins.

  **Step 4:** You will be macro managed, great results are expected and freedoms are given based on great results.

  **Step 5:** Internal coaching is encouraged and we will be constantly provided to you.

  **Step 6:** Enjoy what you are doing, because we do, and get ready to become lean and mean.


Once you're up to speed and entrenched in a project, our first checkpoint will be on your 30th day at Codesmith. 

+ Check in with the leadership team, from operations, product, and engagement. How are things going, and what can we do to improve your visibility into the opportunities at Codesmith? 
+ Swag! You've reached a significant milestone, graduating from orientation mode. We'll swag you up with some team-exclusive perks to celebrate
+ In depth company insight exposure: we'll share this year's company goals, what we're doing to attain those strategic cgoals, and share how you can participate as you become more comfortable in our environment. 

#### day 60 

After two months of slaying code and taking names, you'll be at a great point to start investing in your professional development at Codesmith. This is where we introduce you to your Mentor. 

A mentor serves to be your continued point of contact for professional development, navigating role responsibilities, and gaining technical insight from someone outside of your project peers. 

+ Establish quarterly check-ins with your mentor 
+ Review check-in process, career progression, and mentorship expectations 

At this point, we'll also set up a pair programming based technical review with your Product Lead. This will allow us to share resources for growth, and set goals for your technical growth over the next quarter

#### day 90 

Congrats, you've hit the tail end of onboarding, and you're feeling comfortable as a teammate at Codesmith. We'll do a quick review to celebrate and document accomplishments, as well as discuss your onboarding process with the leadership team. 

+ How is your project going, and are there things you need help with? 
+ What are your thoughts around team collaboration thus far? 
+ Are you on track with the technical growth you envisioned when joining Codesmith? 
+ Gather feedback related to your leadership and Product Lead

Happy dance - *you're full grown*! 
