## Win 

We like winning. We win, ALL the time. We think you can be a part of this team and be a winner. Winning is not a right, and it’s not inevitable; it comes when we give all the ability and effort we have. It comes when we work together. It comes when we do all we can and trust in the outcome. So, go win some.

#### some lingo 

If you haven't seen the "ABC - Always Be Closing" scene from Glengarry Glen Ross, you have to watch it. It's a staple scene in the sales world, and while we don't consider ourselves sales-oriented (we're results-oriented), it's the inspiration behind a phrase you'll hear a lot around Codesmith. Brace yourself: 

   **Closers Be Closing.** 

We know, we're cool. 

We relish in success. It doesn't mean we fear failure; failure leads to innovation, education, and learning best practices. However, we focus on optimizing situations so we can be successful, and so our clients can be successful. We want to deliver what we say we will, and sometimes more. We want to set expectations, and reach or exceeed them. We are closers, and we are always closing. 

----

How are you going to join the ranks of **Closers Be Closing**? We have a few expectations of our team to make sure we can achieve our ambitious goals: 

+ **Honor our core values.** Take responsibility for ensuring that those around you honor them as well. We talk about them all the time. 


+ **Serve our clients.** Clients pay the bills around here. Without them, we don’t have a reason for existing. Serving the clients, solving their problems and answering their questions are our first orders of business. 


+ **Communication,** Communication and more Communication is what we must do.


+ Our clients must know where we are in projects on any given day.


+ **Engage as a person.** Both with our clients and with each other, it is important that we engage person to person.


+ **Think of Codesmith as your business.** Do the right thing and think of the whole, not just yourself. Everything you do reflects not only on you but also on everyone at Codesmith.


+ When problems arise, as they always do, surface the issue as quickly as possible.


+ Talk about problems with people who can solve them, not with people who can’t.


+ You will never be singled out to a client for an issue. **We succeed and fail as a team.**


+ **Ask for help.** It is your responsibility to complete your work on time. If you see a looming deadline in jeopardy, it is your responsibility to call out for help. That’s why you have teammates. Do this far enough in advance that help can be useful and deadlines met. 


+ **Set high standards for yourself** and expect no less from your teammates. Build a business that you will be proud of – get involved. Accept the responsibility to make this a great company; don’t count on someone else to do it for you.


+ Codesmith has to determine through metrics how well individuals and teams are doing, otherwise how can we succeed as a team and a business? So, in Zoho make sure to always keep track of your tasks and time. When clients or leadership need to know how we are doing, we can show them. This recording of data is required for all projects and should equal each day to 8 hours, unless time off was taken in there somewhere.

+ **We depend on each other** for cooperation, feedback, assistance and a quality job. Always strive to do your best and show appreciation to your teammates who are doing the same.

